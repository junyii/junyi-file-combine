# 大黄人的ExcelVBA数据合并器

小程序 `大黄人的数据合并小程序` 用于合并多个Excel文件。

<br>

## 版本信息

版本号 Version：1.1<br>
最终更新日期 Last Update Date：2021-03-05<br>
作者 Author：JunYi<br>
网址 Link：https://gitee.com/junyii/junyi-test-simulator<br>

更新内容 Updates V1.1 2021-03-05：
1. 新增3列自定义添加列，可以在合并数据表时添加在合并结果里，为每个文件添加类似日期、状态等信息。
2. 合并过程中出错不再会打断全部的进程，而是可以选择继续执行后面的文件。并给记录每个文件的执行结果。

更新内容 Updates V1.0 2021-01-16：
1. 完成主版本。

<br>

## Demo

![img](./img/demo.jpg)

<br>

## 关于

- 大部分基础类模块需要用到的代码库来源：https://gitee.com/junyii/vba-code-base
- 遇到任何BUG或有任何建议可以留言。
